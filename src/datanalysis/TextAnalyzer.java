package datanalysis;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TextAnalyzer implements Callable<AnalysisResult> {

	// Class variables
	private String strInput;
	private String classifier;
	final public static Map<String, String> ATTRIBUTE_PATTERNS_MAP;
	static
	{
		ATTRIBUTE_PATTERNS_MAP = new HashMap<String, String>();
		ATTRIBUTE_PATTERNS_MAP.put("vowel anywhere", "(?iu)[aeio]");
		ATTRIBUTE_PATTERNS_MAP.put("vowels within word", "(?iu)[aeio]\\B");
		ATTRIBUTE_PATTERNS_MAP.put("vowels at end of word", "(?iu)[aeio]\\b");
		ATTRIBUTE_PATTERNS_MAP.put("word count", "\\s+");
		ATTRIBUTE_PATTERNS_MAP.put("Number of 'z's", "(?iu)[z]"); // 5
		ATTRIBUTE_PATTERNS_MAP.put("Number of 'v's", "(?iu)[v]");
		ATTRIBUTE_PATTERNS_MAP.put("Number of 'j's", "(?iu)[j]");
		ATTRIBUTE_PATTERNS_MAP.put("Number of 'll's", "(?iu)ll");
		ATTRIBUTE_PATTERNS_MAP.put("Count of 'ie' or 'ei'", "(?iu)[ie]{2}");
		ATTRIBUTE_PATTERNS_MAP.put("Consecutive same vowel", "(?iu)aa|oo|ee");
		ATTRIBUTE_PATTERNS_MAP.put("Count of 'er' at end of word", "(?iu)er\\b");
		ATTRIBUTE_PATTERNS_MAP.put("Words ending in 'ed'", "(?iu)ed\\b"); // 12
		ATTRIBUTE_PATTERNS_MAP.put("Words ending in 'y'", "(?iu)y\\b");   // 13
		ATTRIBUTE_PATTERNS_MAP.put("Number of consecutive constonants", "(?iu)[bcdfghjklmnpqrstvwxz]{4}+");
	}
	final public static String[] ATTRIBUTE_PATTERNS = new String[] 
			{ "(?iu)[aeio]", "(?iu)[aeio]\\B", "(?iu)[aeio]\\b", "\\s+", 
		"(?iu)[z]", "(?iu)[ie]{2}" };

//	{ "(?iu)[aeio]", "(?iu)[aeio]\\B", "(?iu)[aeio]\\b", "\\s+", 
//"(?iu)[z]", "(?iu)[v]", "(?iu)[j]", "(?iu)ll", "(?iu)[ie]{2}", 
//"(?iu)aa|oo|ee", "(?iu)er\\b", "(?iu)ed\\b", "(?iu)y\\b", 
//	"(?iu)[bcdfghjklmnpqrstvwxz]{4}+" };
	
	/**
	 * Two args constructor
	 * @param name the name of the analyzer
	 * @param strText the text to analyze
	 */
	public TextAnalyzer(String name, String strText) {
		this.classifier = name;
		this.strInput = strText;
	}

	/**
	 * Overridden method to serve as a future
	 */
	@Override
	public AnalysisResult call() {

		// Clean up the string
		strInput = deAccent(strInput);
		strInput = stripTheJunk(strInput);
		
		// Array for the attribute values
		double[] attributes = new double[ATTRIBUTE_PATTERNS.length];

		// For each attribute, calculate the count
		for (int i = 0; i < ATTRIBUTE_PATTERNS.length; i++) {
			Pattern ptrn = Pattern.compile(ATTRIBUTE_PATTERNS[i]);
			Matcher matcher = ptrn.matcher(strInput);
			int cnt = 0;
			while (matcher.find()) {
				cnt++;
			}

			attributes[i] = cnt;
		}

		return new AnalysisResult(this.classifier, attributes, this.strInput);

	}
	
	// Replace accented characters with their respective "base cases".
	private String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		//System.out.println("\n\nResults Are: \n" + pattern.matcher(nfdNormalizedString).replaceAll(""));
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	private String stripTheJunk(String str) {
//		return Pattern.compile(Detector.SPECIAL_CHARACTERS)
//				.matcher(str).replaceAll("");
		String retVal = str.replaceAll(Detector.SPECIAL_CHARACTERS, "");
		retVal = retVal.replaceAll("\\s{2,}", " ");
		return retVal;
	}

}
