package datanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

/**
 * TrainingData.java
 * 
 * The goal of this class is...
 * 1. To hold a list of strings, in chronological order, for a data set
 * 2. Be able to read training data from a file or series of files in a folder
 * 3. Be able to provide a string (of random length between x and y) for processing.
 * 
 * 
 * 
 * @author Geoff Berl
 */
public class TrainingData {

	private String identity;
	private String[] wordList;
	
	/**
	 * 
	 * @param folder	location of training source
	 */
	public TrainingData(String identity, String folder) {
		this.identity = identity;

		// Load data from folder
		loadTrainingData();
	}

	// Loads in the data from a file location
	private void loadTrainingData() {
		// This will hold the values until they are read to be split(" ")
		StringBuilder sb = new StringBuilder();
		
		// For each file, collect the data if it matches the identity
		FileInputStream fis = null;
		byte[] data = null;
//		System.out.println("We have " + new File("lib/").listFiles().length + " files");
		for (File file : new File("lib/").listFiles()) {
			// Skip if the left portion of the file is not equal to the identity
//			System.out.println("!" + 
//					file.getName().substring(0,identity.length()) + ".equals(" + identity + ")");
//			System.out.println("Looking at " + file.getName());
			if (!file.getName().substring(0,identity.length()).equals(identity)) {
//				System.out.println("!" + 
//						file.getName().substring(0,identity.length()) + ".equals(" + identity + ")");
				continue;
			}
			// Open and read the file
			try {
				fis = new FileInputStream(file);
				data = new byte[(int)file.length()];
				fis.read(data);
				String input = new String(data, "UTF-8");

				// Replace multiple spaces with one space (so we can delimit on space)
				input = input.replaceAll("\\s{2,}", " ");
//				System.err.println(input);
				

				// If the data is not blank... add it
				if (!input.trim().equals("")) {
					sb.append(input.trim());
					sb.append(" ");
				}
//				System.err.println(sb.length());

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fis.close();
				} catch (IOException e) {
					System.err.println("No bueno, couldn't close the scanner");
					e.printStackTrace();
				}
			}
			// Once we are done, split(" ") the data.
			wordList = sb.toString().trim().split(" ");
		}
	}

	/**
	 * Method to return a string of length (maxSize - minSize)
	 * @param minLen the minimum length of the sample
	 * @param maxLen the maximum length of the sample
	 * @return as string with a random length
	 */
	public String getSample(int minLen, int maxLen) {
		Random rand = new Random(System.nanoTime());

		// Pick a random length
		int strLen = rand.nextInt(maxLen - minLen + 1) + minLen;
//		System.out.println("Picking a random string of length " + strLen);

		// Pick a random start point
		int start = rand.nextInt(wordList.length-strLen-1);

		StringBuilder sb = new StringBuilder();
		for (int i = start; i < start+strLen; i++) {
			sb.append(wordList[i]);
			sb.append(" ");
		}

		return sb.toString();
	}

	public String getIdentity() {
		return identity;
	}


	// Test the class
	public static void main(String[] args) {
		TrainingData[] sampleData = new TrainingData[3];
		sampleData[0] = new TrainingData("du", "lib/");
		sampleData[1] = new TrainingData("it", "lib/");
		sampleData[2] = new TrainingData("en", "lib/");
		
		System.out.println("Random Dutch sample:\n" + sampleData[0].getSample(10, 100) + "\n");
		System.out.println("Random Italian sample:\n" + sampleData[1].getSample(10, 100) + "\n");
		System.out.println("Random English sample:\n" + sampleData[2].getSample(10, 100) + "\n");
	}

}
