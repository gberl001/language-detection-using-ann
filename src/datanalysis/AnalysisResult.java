package datanalysis;

public class AnalysisResult {
	private String id;
	private double[] attributes;
	private String originalString;
	
	
	public AnalysisResult(String id, double[] attributes, String originalString) {
		super();
		this.id = id;
		this.attributes = attributes;
		this.originalString = originalString;
	}

	public String getId() {
		return id;
	}

	public double[] getAttributes() {
		return attributes;
	}

	public String getOriginalString() {
		return originalString;
	}

}
