package datanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Detector {

	// TODO: Strip special characters, numbers and those weird question marks in diamonds.
	final private ExecutorService executor;
	final private ExecutorCompletionService<AnalysisResult> completionService;
	final private static int THREAD_POOL_SIZE = 3;
	public static final boolean NORMALIZE = true;
	public static final String SPECIAL_CHARACTERS = "[^a-zA-Z ]";

	public Detector() {
		// Create executor and wrap in a completion service
		executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		completionService = new ExecutorCompletionService<AnalysisResult>(executor);

		// Ensure that the executor shuts down properly if the
		// application is closed while the threads are running.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				executor.shutdown();
				while (true) {
					try {
						// Wait for the service to exit
						if (executor.awaitTermination(5, TimeUnit.SECONDS)) {
							break;
						}
					} catch (InterruptedException ignored) {
					}
				}
				System.out.println("Closing now");
			}
		}));

	}

	public void beginAnalysis(final String input) {
		Map<String, String> stringsToAnalyze = new HashMap<String, String>();
		// If the input is 'TEST', run in test mode
		if (input.equals("TEST")) {
			// Run in test mode, get data from files
			System.out.println("Running in test mode");
			getContentsFromFiles(stringsToAnalyze, Detector.NORMALIZE);
		} else {
			// Run this single string through the attribute analyzer
			System.out.println("Running in full analysis mode");
			stringsToAnalyze.put("Manual Entry", input);
		}

		// Create and submit the search threads
		for (Map.Entry<String, String> search : stringsToAnalyze.entrySet()) {
			completionService.submit(new TextAnalyzer(search.getKey(), search.getValue()));
		}

		/* Loop through and get results */
		for (int i = 0; i < stringsToAnalyze.size(); i++) {
			Future<AnalysisResult> future = null;
			try {
				// Wait for a result
				future = completionService.take();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			// Retrieve the count of occurrences
			AnalysisResult result = null;
			try {
				result = future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			// Print the results of this query
//			System.out.println();

		}

		// The executor can safely be shut down.
		executor.shutdown();
	}

	private void getContentsFromFiles(Map<String, String> lst
			, boolean normalize) {
		// For each sample file, run attributes
		FileInputStream fis = null;
		byte[] data = null;
		for (File file : new File("lib/").listFiles()) {
			// Open and read the file
			try {
				fis = new FileInputStream(file);
				data = new byte[(int)file.length()];
				fis.read(data);

				// Add the data to the array
				System.out.println("\nThe Fixed data looks like this: \n");
				System.err.println(deAccent(stripTheJunk(new String(data, "UTF-8"))));
				if (normalize) {
					lst.put(file.getName(), 
							deAccent(stripTheJunk(new String(data, "UTF-8"))));
				}else {
					lst.put(file.getName(), 
							stripTheJunk(new String(data, "UTF-8")));
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fis.close();
				} catch (IOException e) {
					System.err.println("No bueno, couldn't close the scanner");
					e.printStackTrace();
				}
			}

		}
	}

	// Replace accented characters with their respective "base cases".
	private String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		//System.out.println("\n\nResults Are: \n" + pattern.matcher(nfdNormalizedString).replaceAll(""));
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	private String stripTheJunk(String str) {
//		return Pattern.compile(Detector.SPECIAL_CHARACTERS)
//				.matcher(str).replaceAll("");
		String retVal = str.replaceAll(Detector.SPECIAL_CHARACTERS, "");
		retVal = retVal.replaceAll("\\s{2,}", " ");
		return retVal;
	}

}
