package datanalysis;

import java.io.FileNotFoundException;

public class Driver {

	/**
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) {
		//Check if no args
		if (args.length != 1) {
			System.err.println("Usage: java Driver <text string>");
			System.exit(1);
		}

		// Run the program
		new Detector().beginAnalysis(args[0]);
	}
}
