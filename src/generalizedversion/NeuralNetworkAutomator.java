package generalizedversion;

import java.util.Scanner;

/**
 * The intent of this class is to
 * 	Generate a random set of sample inputs
 * 	Generate a few networks with various configurations
 * 	Train each network with the same sample inputs and print results
 * 
 * @author Geoff Berl
 */
public class NeuralNetworkAutomator {

	public static final int[] hiddenLayerSizes = 
			new int[] { 4, 5, 6, 7, 8, 9, 10 };
	public static final double[] learningRateRange = new double[] {
		0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 };
	private static int inputCnt = TextAnalyzer.ATTRIBUTE_PATTERNS.length;

	public static void main(String[] args) throws InterruptedException {

		testLearningRates();

	}

	public static void testLearningRates() throws InterruptedException {
		// Variables
		double minError = 0.01;
		long maxEpochs = 100000;


		// Get sample inputs


		// Training Data
		double[][] input = null; // = ...
		double[][] expected = null; // = ...

		// Ask the user for a learning rate, then run each network
		Scanner in = new Scanner(System.in);
		String userInput = "";
		try {
			System.out.print("Enter a learning rate: ");
			while(!(userInput = in.nextLine()).isEmpty()) {
				// Convert input to double
				double rate = Double.parseDouble(userInput);

				// run each network with the learning rate and print results
				NeuralNetwork net1 = new NeuralNetwork(
						new int[] {inputCnt, 8, 3} , input, expected, rate, minError, maxEpochs);
				net1.start();
				net1.join();
				System.out.println("Results for net1\n" + net1 + "\n");

				NeuralNetwork net2 = new NeuralNetwork(
						new int[] {inputCnt, 7, 3} , input, expected, rate, minError, maxEpochs);
				net2.start();
				net2.join();
				System.out.println("Results for net2\n" + net2 + "\n");

				NeuralNetwork net3 = new NeuralNetwork(
						new int[] {inputCnt, 6, 3} , input, expected, rate, minError, maxEpochs);
				net3.start();
				net3.join();
				System.out.println("Results for net3\n" + net3 + "\n");

				NeuralNetwork net4 = new NeuralNetwork(
						new int[] {inputCnt, 5, 3} , input, expected, rate, minError, maxEpochs);
				net4.start();
				net4.join();
				System.out.println("Results for net4\n" + net4 + "\n");

				NeuralNetwork net5 = new NeuralNetwork(
						new int[] {inputCnt, 4, 3} , input, expected, rate, minError, maxEpochs);
				net5.start();
				net5.join();
				System.out.println("Results for net5\n" + net5 + "\n");

				System.out.println("");
				System.out.print("Enter a learning rate: ");
			}
		} finally {
			in.close();
		}
	}

}
