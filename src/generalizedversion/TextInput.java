package generalizedversion;

public class TextInput {

	private String id;
	private String input;
	private String assumedClass;
	
	
	public TextInput(String id, String input, String assumedClass) {
		super();
		this.id = id;
		this.input = input;
		this.assumedClass = assumedClass;
	}


	public String getId() {
		return id;
	}


	public String getInput() {
		return input;
	}


	public String getAssumedClass() {
		return assumedClass;
	}


	@Override
	public String toString() {
		return "TextRecord [id=" + id + ", input=" + input + ", assumedClass="
				+ assumedClass + "]";
	}

}
