package generalizedversion;

/**
 * TextAnalysisResult.java
 * 
 * This class represents a classification and results array to be used with
 * the TextAnalyzer object.
 * 
 * @author Geoff Berl
 */
public class TextInputResult {
	
	private String id;
	private String input;
	private String assumedClass;
	private double[] attributes;

	public TextInputResult(String id, String input, String assumedClass,
			double[] attributes) {
		super();
		this.id = id;
		this.input = input;
		this.assumedClass = assumedClass;
		this.attributes = attributes;
	}


	public String getId() {
		return id;
	}


	public String getInput() {
		return input;
	}


	public String getAssumedClass() {
		return assumedClass;
	}

	/**
	 * Get the array of attribute counts
	 * @return the array of attribute counts
	 */
	public double[] getAttributes() {
		return attributes;
	}

}