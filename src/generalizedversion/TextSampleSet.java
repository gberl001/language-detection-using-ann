package generalizedversion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * TextSample.java
 * 
 * This class is designed to hold sample input data for training a neural
 * network.  In addition, this class can be fed strings of input to be
 * analyzed and will process them and add them to the data set.
 * 
 * @author Geoff Berl
 */
public class TextSampleSet {

	private String identity;
	private String[] wordList;
	final public static String[] ATTRIBUTE_PATTERNS = new String[] 
			{ "(?iu)[aeio]", "(?iu)[aeio]\\B", "(?iu)[aeio]\\b", "\\s+", 
			"(?iu)[z]", "(?iu)[v]", "(?iu)[ie]{2}", "(?iu)aa|oo|ee" };
	private String[] replacePtrns = new String[] { "\\bCSR'?S\\b", "\\bEST\\b",
			 "\\bFLEX-?FIELD\\b", "\\bPS\\b", "\\bQTY\\b", "\\bREQ('?TS?)?\\b", 
			 "\\bREV\\b", "\\bROCHESTER\\\\MS\\b", "\\bSB'?S?\\b", "\\bSN'?S?\\b", 
			 "\\bSPECS\\b", "\\bNWARR?(ANTY)?\\b", "\\bWARR\\b", "\\bNPF\\b", 
			 "\\bP/?N\\b", "\\b(PHOTOS?|PICS?|PICTURES)\\b", "\\bTECH\\b", 
			 "\\bASSY\\b", "[\\n\\r]", "[^A-Za-z\\s']", "\\s{2,}", "\\bMATL\\b", 
			 "C\\s?OF\\s?C", "\\bACC'?S?\\b", "AMAT", "\\bDASH\\b" };
	private String[] replaceStrs = new String[] { "CSR", "ESTIMATE", 
			"FLEX FIELD", "POWER SUPPLY", "QUANTITY", "REQUIREMENT", "REVISION", 
			"ROCHESTER", "SERVICE BULLETIN", "SERIAL NUMBER", "SPECIFICATION", 
			"NON WARRANTY", "WARRANTY", "NO PROBLEM FOUND", "PART NUMBER",
            "PICTURE", "TECHNICIAN", "ASSEMBLY", " ", " ", " ", "Material", 
            "CofC", "ACCESSORIES", "APPLIED MATERIALS", "Dashboard" };
	private String removePtrn = "\b(mon|tues|wed|thurs|fri|sat|sun)(day)?\b" +
               "|\b(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)(uary|ruary|ch|il|e|y|ust|tember|tober|ember)?\b" +
               "|'";
	private List<TextInput> inputRecords;
	private ExecutorService executor;

	
	/**
	 * 
	 * @param folder	location of training source
	 */
	public TextSampleSet(String identity, String folder) {
		this.identity = identity;

		// Load data from folder
		loadTrainingData();
	}
	
	public TextSampleSet(String name) {
		this.identity = name;
		executor = Executors.newFixedThreadPool(
				Runtime.getRuntime().availableProcessors());
		
		// Ensure that the executor shuts down properly if the
		// application is closed while the threads are running.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				executor.shutdown();
				while (true) {
					try {
						// Wait for the service to exit
						if (executor.awaitTermination(5, TimeUnit.SECONDS)) {
							break;
						}
					} catch (InterruptedException ignored) {
					}
				}
				System.out.println("Closing now");
			}
		}));
	}
	
	
	public void addRecord(TextInput input) {
		if (inputRecords == null) {
			inputRecords = new ArrayList<TextInput>();
		}
		
	}
	
	

	// Loads in the data from a file location
	private void loadTrainingData() {
		// This will hold the values until they are read to be split(" ")
		StringBuilder sb = new StringBuilder();
		
		// For each file, collect the data if it matches the identity
		FileInputStream fis = null;
		byte[] data = null;

		for (File file : new File("lib/").listFiles()) {
			// Skip if the left portion of the file is not equal to the identity
			if (!file.getName().substring(0,identity.length()).equals(identity)) {
				continue;
			}
			// Open and read the file
			try {
				fis = new FileInputStream(file);
				data = new byte[(int)file.length()];
				fis.read(data);
				String input = new String(data, "UTF-8");

				// Replace multiple spaces with one space (so we can delimit on space)
				input = input.replaceAll("\\s{2,}", " ");				

				// If the data is not blank... add it
				if (!input.trim().equals("")) {
					sb.append(input.trim());
					sb.append(" ");
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fis.close();
				} catch (IOException e) {
					System.err.println("No bueno, couldn't close the scanner");
					e.printStackTrace();
				}
			}
			// Once we are done, split(" ") the data.
			wordList = sb.toString().trim().split(" ");
		}
	}

	/**
	 * Method to return a string of length (maxSize - minSize)
	 * @param minLen the minimum length of the sample
	 * @param maxLen the maximum length of the sample
	 * @return as string with a random length
	 */
	public String getSample(int minLen, int maxLen) {
		Random rand = new Random(System.nanoTime());

		// Pick a random length
		int strLen = rand.nextInt(maxLen - minLen + 1) + minLen;

		// Pick a random start point
		int start = rand.nextInt(wordList.length-strLen-1);

		StringBuilder sb = new StringBuilder();
		for (int i = start; i < start+strLen; i++) {
			sb.append(wordList[i]);
			sb.append(" ");
		}

		return sb.toString();
	}

	public String getIdentity() {
		return identity;
	}

}
