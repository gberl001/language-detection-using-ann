package main;

import java.text.DecimalFormat;

import neuralnetwork.MyNetwork;

public class XORTest {


	public static void main(String[] args) {
		MyNetwork net = new MyNetwork();

		// *** Paste material from comments below

		// Possible inputs for an XOR
		double[][] input = new double[][] { { 0.0, 0.0 }, { 1.0, 0.0 }, { 0.0, 1.0 }, { 1.0, 1.0 } };
		// Expected outputs for an XOR
		double[][] expected = new double[][] { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
		// Neurons/layer count
		int[] neuronCounts = new int[] {2, 3, 1};

		// *** END paste material... ******

		// Create the network
		net.setupNetwork(neuronCounts, input, expected, 0.7, 0.001, 50000);

		// Start the thread
		net.start();

		// Wait for the thread to finish
		try {
			net.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\nTraining is complete.");
		System.out.println("Total Error is " + net.getTotalError());

		// Print out a test result
		System.out.println("\nNow... test the trained network:");
		for (int i = 0; i < expected.length; i++) {
			final double actual[] = net.computeOutput(input[i]);
			System.out.println(input[i][0] + "," + input[i][1]
					+ ", computed: " + new DecimalFormat("0.00").format(actual[0]) 
					+ ", expected: " + expected[i][0]);
		}
		
		System.out.println("\n" + net.toString());
	}

	// *************XOR material ******
	//	// Possible inputs for an XOR
	//	double[][] input = new double[][] { { 0.0, 0.0 }, { 1.0, 0.0 }, { 0.0, 1.0 }, { 1.0, 1.0 } };
	//	// Expected outputs for an XOR
	//	double[][] expected = new double[][] { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
	//	// Neurons/layer count
	//	int[] neuronCounts = new int[] {2, 3, 1};
	// END ******** XOR material ******
}
