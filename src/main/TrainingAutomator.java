package main;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import datanalysis.AnalysisResult;
import datanalysis.TextAnalyzer;
import datanalysis.TrainingData;

/**
 * TrainingAutomator.java
 * 
 * The goal of this class is to, given a number of samples...
 * 	Create a training set with that many samples
 * 	Create numNetworks ANNs with varying hidden layer node counts
 * 	Train said networks with numVariations varied learning rates
 * 	Compile the results for each ANN and it's varying results
 * 
 * This should allow us to figure out two things
 * 	If this sample size is large enough
 * 	Which network variation results in the best outcome (will need to be replicated to prove)
 * 
 * @author Geoff Berl
 */
public class TrainingAutomator {

	final private static int THREAD_POOL_SIZE = 4;
	final private ExecutorService executor;
	final private ExecutorCompletionService<AnalysisResult> completionService;
	private double[][] inputData;
	private double[][] expectedOutput;
	private double[][] minimax;

	public TrainingAutomator(int numSamples, int numNetworks, int numVariations) {
		// Create executor and wrap in a completion service
		executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		completionService = new ExecutorCompletionService<AnalysisResult>(executor);

		// Ensure that the executor shuts down properly if the
		// application is closed while the threads are running.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				executor.shutdown();
				while (true) {
					try {
						// Wait for the service to exit
						if (executor.awaitTermination(5, TimeUnit.SECONDS)) {
							break;
						}
					} catch (InterruptedException ignored) {
					}
				}
				System.out.println("Closing now");
			}
		}));

		// Create training sample data
		inputData = new double[numSamples][TextAnalyzer.ATTRIBUTE_PATTERNS.length];
		expectedOutput = new double[numSamples][3];
		setupSampleData(numSamples);

//		// Print out results to verify
//		System.out.println("Done setting up samples");
//		System.out.println("inputData contains " + inputData.length + " records");
//		for (int i = 0; i < inputData.length; i++) {
//			System.out.print(expectedOutput[i][0] + ", " + expectedOutput[i][1] + ", " + expectedOutput[i][2] + "\t");
//			for (int j = 0; j < inputData[0].length; j++) {
//				System.out.print(inputData[i][j] + "\t");
//			}
//			System.out.println();
//		}
	}

	private void setupSampleData(int nSamples) {
		//	Get TraininigData for each
		TrainingData[] sampleData = new TrainingData[3];
		sampleData[0] = new TrainingData("du", "lib/");
		sampleData[1] = new TrainingData("it", "lib/");
		sampleData[2] = new TrainingData("en", "lib/");	

		// For each sample set, generate nSamples
		String[] sampleInputs = new String[nSamples];
		String[] sampleClasses = new String[nSamples];	
		int segmentSize = (nSamples/sampleData.length);
		for (int i = 0; i < sampleData.length; i++) {
			int offset = segmentSize*i;
			for (int j = 0; j < segmentSize; j++) {
				// Get a sample string from 10 to 100 characters
				sampleInputs[j + offset] = sampleData[i].getSample(10, 100);
				sampleClasses[j + offset] = sampleData[i].getIdentity();
			}
		}
		//		System.out.println("We have training data containing " + sampleInputs.length + " records");

		// Create and submit the search threads to get attributes
		for (int i = 0; i < sampleInputs.length; i++) {
			completionService.submit(new TextAnalyzer(sampleClasses[i], sampleInputs[i]));
		}

		/* Loop through and get results */
		minimax = new double[inputData[0].length][2];
		for (int i = 0; i < sampleInputs.length; i++) {
			Future<AnalysisResult> future = null;
			try {
				// Wait for a result
				future = completionService.take();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			// Retrieve the count of occurrences
			AnalysisResult result = null;
			try {
				result = future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			// Get minimax values
			for (int j = 0; j < minimax.length; j++) {
				if (result.getAttributes()[j] < minimax[j][0]) {
					minimax[j][0] = result.getAttributes()[j];
				}
				if (result.getAttributes()[j] > minimax[j][1]) {
					minimax[j][1] = result.getAttributes()[j];
				}
			}

			// Add input array
			inputData[i] = result.getAttributes();

			// Generate and add output array
			double[] expOut = new double[sampleData.length];
			for (int j = 0; j < expOut.length; j++) {
				if (result.getId().equals(sampleData[j].getIdentity())) {
					expOut[j] = 1;
				} else {
					expOut[j] = 0;
				}
			}
			expectedOutput[i] = expOut;
		}

		// Now we must loop through all of the data to normalize it
		for (int i = 0; i < inputData.length; i++) {
			normalizeInput(inputData[i]);
//			for (int j = 0; j < inputData[0].length; j++) {
//				inputData[i][j] = (inputData[i][j]-minimax[j][0])
//						/(minimax[j][1]-minimax[j][0]);
//			}
		}

		// The executor can safely be shut down.
		executor.shutdown();
	}

	public double[][] getInputData() {
		return this.inputData;
	}

	public double[][] getExpectedOutputs() {
		return this.expectedOutput;
	}
	
	public void normalizeInput(double[] input) {
		// Normalize each feature
		for (int i = 0; i < input.length; i++) {
			input[i] = (input[i]-minimax[i][0])/(minimax[i][1]-minimax[i][0]);
		}
	}

}
