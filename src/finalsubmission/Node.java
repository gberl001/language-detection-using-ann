package finalsubmission;

import java.util.Random;

/**
 * Node.java
 * 
 * A simple class that holds the weights of incoming connections from previous
 * nodes.  The weights are initialized with random values between -1 and 1 
 * upon instantiation. 
 * 
 * @author Geoff Berl
 */
public class Node {

	private double[] weights;	// The weights this node
	private double output;		// The output value of this node
	private double error; 		// The error for this node	

	/**
	 * Single arg constructor
	 * @param inputs the number of inputs for this node
	 */
	public Node(int inputs) {
		// Create the weights array
		weights = new double[inputs];

		// Set initial random weights
		initializeWeights();
	}

	/**
	 * Initializes the weights with random values
	 */
	private void initializeWeights() {
		Random rand = new Random(System.currentTimeMillis());

		// Create random weights between -1 and 1
		for (int i = 0; i < weights.length; i++) {
			weights[i] = rand.nextDouble()*2.0 - 1.0;
		}
	}


	//****************************************************\\
	//*******************GETTERS**************************\\
	//****************************************************\\

	/**
	 * Get the weights of inputs from previous layer.  These weights are
	 * randomly set on initialization of the Node.
	 * @return an array of weights respective to the number of inputs for this node
	 */
	public double[] getWeights() {
		return weights;
	}

	/**
	 * Get the output of this node.  This value is to be updated by a parent
	 * class during the feed forward operation
	 * @return the output value
	 */
	public double getOutput() {
		return output;
	}
	
	/**
	 * Get the error of this node.  This value is to be updated by a parent
	 * class during the backpropagation operation
	 * @return the output value
	 */
	public double getError() {
		return error;
	}
	

	//****************************************************\\
	//*******************SETTERS**************************\\
	//****************************************************\\

	/**
	 * Set the weights for the inputs to this node.  This is to be called
	 * by a parent class performing feed forward
	 * @param weights the array of new weight values
	 */
	public void setWeights(double[] weights) {
		this.weights = weights;
	}
	
	/**
	 * Set the error of this Node
	 * @param output the output value of this node
	 */
	public void setError(double error) {
		this.error = error;
	}
	
	/**
	 * Set the output of this Node
	 * @param output the output value of this node
	 */
	public void setOutput(double output) {
		this.output = output;
	}

}
