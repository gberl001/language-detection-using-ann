package finalsubmission;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Driver {;
	
	public static void main(String[] args) throws InterruptedException {

		TrainingSetGenerator ta = new TrainingSetGenerator(381, "lib/");

		// Training Data
		double[][] input = ta.getInputData();
		double[][] expected = ta.getExpectedOutputs();

		// Create the network
		System.out.println("Training the network, please wait...");
		NeuralNetwork net = new NeuralNetwork(
				new int[] {TextAnalyzer.ATTRIBUTE_PATTERNS.length, 7, 3}
				, input, expected, 0.78, 0.001, 100000);

		// Start the thread and wait for the thread to finish
		net.start();
		net.join();

		System.out.println("\nTraining is complete with an error of " 
				+ net.getTotalError() + "\n");

		// now let the user enter some data to test the network
		Scanner in = new Scanner(System.in);
		ExecutorService executor = Executors.newFixedThreadPool(1);
		String userInput = "";
		System.out.println("*** NOTE: At any time, leave the entry blank to quit ***");
		System.out.print("Enter a string: ");
		try {
			while(!(userInput = in.nextLine()).isEmpty()) {
				
				// Analyze the text
				TextAnalyzer textAnalyzer = new TextAnalyzer("Manual",userInput);
				Future<TextAnalysisResult> analysis = executor.submit(textAnalyzer);
				double[] attrib = null;
				try {
					attrib = analysis.get().getAttributes();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				ta.normalizeInput(attrib);
				
				// Run entry through network
				final double[] result = net.computeOutput(attrib);
				
				// Print the Expected values
				System.out.println("Calculated Language: " + ta.getWinner(result) + "\n");

				// Ask for another entry
				System.out.print("Enter another string?: ");
			}
		} finally {
			in.close();
		}

		executor.shutdown();

	}
}
