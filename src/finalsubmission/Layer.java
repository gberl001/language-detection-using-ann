package finalsubmission;

/**
 * Layer.java
 * 
 * This class represents a layer in a feed forward neural network.  It holds 
 * objects representing the nodes of the layer and computes the "activation" 
 * of the layer.  It also contains methods to peform functions on all nodes
 * within the layer.
 * 
 * @author Geoff Berl
 */
public class Layer {

	private Node[] nodes;		// The nodes for this layer
	private double[] inputs;	// The inputs from the previous layer

	/**
	 * Two arg constructor creates a feed-forward network layer
	 * @param numNodes the number of nodes in the layer
	 * @param numInputs the number of inputs from previous layer
	 */
	public Layer(int numNodes, int numInputs) {
		// Create an array for the nodes
		nodes = new Node[numNodes];

		// Initialize the nodes with the number of inputs
		for (int i = 0; i < numNodes; i++) {
			nodes[i] = new Node(numInputs);
		}

		// Create the array of inputs from previous layer
		inputs = new double[numInputs];
	}

	/**
	 * Calculates the outputs for nodes in the layer, should be called
	 * for each layer in the network to perform feed forward.
	 */
	public void feedForward() {

		for (int i = 0; i < nodes.length; i++) {
			// Get the current node's threshold
			double output = 0;  //nodes[i].getThreshold();

			// For each weight in the node, compute the change
			for (int j = 0; j < nodes[i].getWeights().length; j++) {
				output += inputs[j] * nodes[i].getWeights()[j];
			}

			// Set the outputs of the node using sigmoid function [g(x)]
			nodes[i].setOutput(1/(1+Math.exp(-output)));
		}
	}

	/**
	 * Get the nodes for the layer 
	 * @return an array of nodes for this layer
	 */
	public Node[] getNodes() {
		return nodes;
	}

	/**
	 * Get the inputs for this layer
	 * @return an array of inputs for this layer
	 */
	public double[] getNodeInputs() {
		return inputs;
	}

	/**
	 * Set the inputs for this layer (from previous layer) should be called by
	 * the network after getting outputs from previous layer during the 
	 * feed forward algorithm.
	 * @param input an array of inputs for this layer
	 */
	public void setInputs(double[] input) {
		this.inputs = input;
	}
	
	/**
	 * Get an array of outputs from this layer
	 * @return an array of outputs
	 */
	public double[] getNodeOutputs() {

		double[] retVal = new double[nodes.length];

		for (int i=0; i < nodes.length; i++) {
			retVal[i] = nodes[i].getOutput();
		}

		return (retVal);
	}
	
	/**
	 * Get the number of nodes in this layer
	 * @return the number of nodes in this layer
	 */
	public int getNumberOfNodes() {
		return nodes.length;
	}
}
