package finalsubmission;

import java.text.Normalizer;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TextAnalyzer.java
 * 
 * The intent of this class is to allow for the analyzing of text in a callable
 * so that it can be used in a concurrent system analyzing multiple text inputs
 * at a time.  Each string will be analyzed using a predetermined set of
 * attributes and will allow for the retrieval of results through an 
 * AnalysisResult object providing the count of occurrences for each attribute.
 * 
 * @author Geoff Berl
 */
public class TextAnalyzer implements Callable<TextAnalysisResult> {

	// Class variables
	private String strInput;
	private String classifier;
	public static final String SPECIAL_CHARS = "[^a-zA-Z ]";
	public static final String ACRONYMS = "[A-Z]{2,}";
	final public static String[] ATTRIBUTE_PATTERNS = new String[] 
			{ "(?iu)[aeio]", "(?iu)[aeio]\\B", "(?iu)[aeio]\\b", "\\s+", 
			"(?iu)[z]", "(?iu)[v]", "(?iu)[ie]{2}", "(?iu)aa|oo|ee" };
	
	/**
	 * Two args constructor
	 * @param name		the class name for the given string
	 * @param strText 	the text to analyze
	 */
	public TextAnalyzer(final String name, final String strText) {
		this.classifier = name;
		this.strInput = strText;
	}

	/**
	 * Overridden method to serve as a future
	 */
	@Override
	public TextAnalysisResult call() {

		// Clean up the input string
		strInput = deAccent(strInput);
		strInput = stripTheJunk(strInput);
		
		// Array for the attribute values
		double[] attributes = new double[ATTRIBUTE_PATTERNS.length];

		// For each attribute, calculate the count
		for (int i = 0; i < ATTRIBUTE_PATTERNS.length; i++) {
			Pattern ptrn = Pattern.compile(ATTRIBUTE_PATTERNS[i]);
			Matcher matcher = ptrn.matcher(strInput);
			int cnt = 0;
			while (matcher.find()) {
				cnt++;
			}

			attributes[i] = cnt;
		}

		return new TextAnalysisResult(this.classifier, attributes, this.strInput);

	}
	
	// Replace accented characters with their respective "base cases".
	private String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	private String stripTheJunk(String str) {
		// Special Characters --> Acronyms is significant because USA and U.S.A is covered where the other way it is not true.
		String retVal = str.replaceAll(SPECIAL_CHARS, "");
		retVal = retVal.replaceAll(ACRONYMS, "");
		retVal = retVal.replaceAll("\\s{2,}", " ");
		return retVal;
	}

}
