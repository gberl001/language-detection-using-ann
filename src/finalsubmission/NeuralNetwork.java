package finalsubmission;

import java.text.DecimalFormat;

/**
 * NeuralNetwork.java
 * 
 * A class to simulate a Feed-forward network using backpropagation learning
 * techniques.  This class is also a thread, really just for training purposes
 * so that we can run multiple cases to figure out the best case.
 * 
 * @author Geoff Berl
 */
public class NeuralNetwork extends Thread {

	// Training criteria
	private double	minError;			// The smallest acceptable error
	private long	maxEpochs;			// Stop after this many epochs
	private boolean poisonPill = false; // A "notification" that the thread should stop
	private double	learningRate;		// The learning rate (try many different)
	
	// Training Data
	private double[][]	expectedOutput;	// Expected outputs with respect to training data inputs
	private double[][] trainingData;	// The input data from which to train

	// Network variables
	private Layer[] layers;				// The layers of the network
	private double[][] actualOutput;	// Stored output for computing total error
	private long epochCnt = 0;			// The total epochs needed to train

	/**
	 * A no argument constructor.  The network will need to be configured
	 * before it can start training.
	 */
	public NeuralNetwork() {
		super();
		// Setup will be called later
	}

	/**
	 * Multiple argument constructor - This constructor completely configures the
	 * neural network with the parameters passed in.  The training algorithm
	 * will run until either the minError is met or beat OR the maximum number
	 * of epochs has been reached, whichever is first.
	 * @pre the network must be configured before training it.
	 * @pre The layers must be in order from input [0] to hidden [length-1]
	 * @param nodesPerLayer	the number of nodes per layer
	 * @param inputSamples the input sample data
	 * @param expectedOutputs the expected outputs of the sample data
	 * @param learnRate the learning rate of the training algorithm
	 * @param minError the minimum error, the error to attempt to meet or beat
	 * @param maxEpochs the maximum number of epochs before finishing training
	 */
	public  NeuralNetwork(int[] nodesPerLayer, double[][] inputSamples, 
			double expectedOutputs[][], double learnRate, double minError, 
			long maxEpochs) {
		this();
		// Set up the network since they used the full constructor
		setupNetwork(nodesPerLayer, inputSamples, expectedOutputs,
				learnRate, minError, maxEpochs);
	}

	/**
	 * Call this class if you initialized a no args Network.  You cannot run
	 * the training algorithm without calling this first.  This is called
	 * automatically if you chose to initialize the Network using the 
	 * parameterized constructor so there is no need to call again.  However,
	 * calling this again will not have ill-effects.
	 * @pre The layers must be in order from input [0] to hidden [length-1]
	 * @param nodesPerLayer	an array containing the number of nodes for each layer
	 * @param inputSamples		the provided training data
	 * @param expectedOutputs	the expected outputs from training data
	 * @param learnRate			the rate at which the network learns
	 * @param minError			the error to beat
	 * @param maxEpochs			the maximum number of epochs to perform
	 */
	public void setupNetwork(int[] nodesPerLayer, double[][] inputSamples, 
			double[][] expectedOutputs, double learnRate, double minError, 
			long maxEpochs) {

//		/** Check validity before continuing */
//		if (!validateNetworkData(numNodesInLayer, inputSamples, expectedOutputs)) {
//			return;
//		}

		// Set up the variables
		this.minError = minError;
		this.learningRate = learnRate;
		this.maxEpochs = maxEpochs;
		layers = new Layer[nodesPerLayer.length];
		this.expectedOutput = expectedOutputs;
		this.trainingData = inputSamples;
		// Create an array to hold the computed output values.
		actualOutput = new double[inputSamples.length][expectedOutput[0].length];

		// Create layers and assign number of nodes to each layer
		for (int i = 0; i < layers.length; i++)  {
			switch(i) {
			case 0: 
				// The input layer has equally many inputs as outputs.
				layers[i] = new Layer(nodesPerLayer[i], nodesPerLayer[i]);
				break;
			default:
				// All other layers have as many inputs as the previous layer has outputs.
				layers[i] = new Layer(nodesPerLayer[i], nodesPerLayer[i-1]);
				break;
			}
		}

	}
	
	/**
	 * Set the learning rate
	 * @param rate the value of the learning rate
	 */
	public void setLearningRate(double rate) {
		this.learningRate = rate;
	}

	/**
	 * Compute and update the activation for each node in the network
	 */
	public void feedForward(){
		// TODO: Update comments for this method
		// Since no weights contribute to the output from the input layer, 
		// assign the input from the input layer to all the nodes in the 
		// first hidden layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getNodes()[i].setOutput(layers[0].getNodeInputs()[i]);
		}
		layers[1].setInputs(layers[0].getNodeOutputs());

		// For the remaining layers, call feed forward for the layer (i)
		// to compute its outputs and then assign those outputs to the next 
		// layer (i+1).
		for (int i = 1; i < layers.length; i++) {
			layers[i].feedForward();

			// When we reach the end, there are no more layers to insert outputs to.
			// so we simply set the inputs rather than feed forward
			if (i != layers.length-1) {
				layers[i+1].setInputs(layers[i].getNodeOutputs());
			}
		}

	}

	/**
	 * A method to return a result from a single input.  Typically this would 
	 * be used once the network is trained.
	 * @param input	the single input to analyze
	 * @return	an array containing the output values.
	 */
	public double[] computeOutput(double[] input) {
		double[] retVal = new double[layers[layers.length-1].getNumberOfNodes()];

		// Set the inputs for the input layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getNodeInputs()[i] = input[i];
		}

		// Calculate activation values
		feedForward();

		// Get the outputs
		for (int i = 0; i < layers[layers.length-1].getNumberOfNodes(); i++) {
			retVal[i] = layers[layers.length-1].getNodes()[i].getOutput();
		}

		return retVal;
	}

	/**
	 * Computes the error (for a single iteration) for each node in the network
	 * @param expected an array of expected outputs for the network layers
	 */
	private void computeNodeErrors(double[] expected) {

		int opLayer;

		opLayer = layers.length-1;

		// Calculate error for output layer nodes
		double opNodeActual;
		for (int i = 0; i < layers[opLayer].getNumberOfNodes(); i++) {
			opNodeActual = layers[opLayer].getNodes()[i].getOutput();

			// TODO: This is supposed to be the g'(x) calculation
			// Calculating the output layer error
			layers[opLayer].getNodes()[i].setError(
					(expected[i] - opNodeActual) * opNodeActual 
					* (1-opNodeActual));
		}

		// Calculate error for nodes in other layers
		double deltaK;
		for (int i = opLayer-1; i > 0; i--) {
			// For every node in the layer...
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				deltaK = 0;

				// add up the weights for every node in the next (output) layer
				// Known as delta[k] for the output layer
				for (int k = 0; k < layers[i+1].getNumberOfNodes(); k++) {
					deltaK += layers[i+1].getNodes()[k].getWeights()[j] 
							* layers[i+1].getNodes()[k].getError();
				}

				// Set the error for this node (O[ij]*(1-O[ij])*w[kj])
				// Known as delta[k] for a hidden layer
				layers[i].getNodes()[j].setError(
						layers[i].getNodes()[j].getOutput() 
						* (1 - layers[i].getNodes()[j].getOutput())
						* deltaK);

			}
		}

	}

	/**
	 * Propagates the error through the network (backwards of course)
	 */
	private void BackPropagateError() {

		// Update weights for every layer
		for (int i = layers.length-1; i > 0; i--) {
			// Update weights for every node in the layer
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				// Update weights
				for (int k = 0; k < layers[i-1].getNumberOfNodes(); k++) {

					// Get the difference between the weights (delta k)
					double weightDiff = learningRate 
							* layers[i].getNodes()[j].getError() 
							* layers[i-1].getNodes()[k].getOutput();

					// Update the weight weight
					layers[i].getNodes()[j].getWeights()[k] += weightDiff;
				}
			}
		}
	}

	/**
	 * Returns the total error for the network
	 * @return the total network error.
	 */
	public double getTotalError() {

		double totalError = 0;
		// For each output of the training data...
		for (int i = 0; i < actualOutput.length; i++) {
			// For each node
			for (int j = 0; j < layers[layers.length-1].getNumberOfNodes(); j++) {
				totalError += Math.pow((expectedOutput[i][j] - actualOutput[i][j]),2);
			}
		}
		return totalError*0.5;
	}

	/**
	 * A method to train the network.  This method will run through the 
	 * activation function, error calculation and weight updates for each
	 * sample case.  The training stops when the max epochs count is reached or
	 * the error is less than the minimum provided error.  The computation may
	 * also be forced to stop by setting a "poisonPill" object to true.
	 */
	public void train() {
		
		while (!poisonPill && (getTotalError() > minError) 
				&& (epochCnt < maxEpochs)) {

			// For each input record
			for (int in = 0; in < trainingData.length; in++) {
				// Get the sample data for this iteration
				double[] currentInput = trainingData[in];
				double[] currentExpected = expectedOutput[in];

				// Set the inputs for the input layer
				for (int j = 0; j < layers[0].getNumberOfNodes(); j++) {
					layers[0].getNodeInputs()[j] = currentInput[j];
				}

				// Always move forward... no matter what happens
				feedForward();

				// keep track of outputs for calculating the total error.
				for (int j = 0; j < layers[layers.length-1].getNumberOfNodes(); j++) {
					actualOutput[in][j] = 
							layers[layers.length-1].getNodes()[j].getOutput();
				}

				// Call a training iteration
				computeNodeErrors(currentExpected);
				BackPropagateError();
			}
			// Print out some helpful information
//			System.out.println("Epoch " + currentEpoch + 
//					": Error=" + getTotalError());

			epochCnt++; // Increment epoch count
		} 

	}

	/**
	 * A toString() method, this method will provide a printable representation
	 * of the network and its given weights
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.00");
		
		sb.append("Total Error: ").append(getTotalError());
		sb.append("\nTotal Epochs: ").append(epochCnt).append(" (max==")
			.append(maxEpochs).append(")\n");
		
		// For each layer
		for (int i = 0; i < layers.length; i++) {
			Layer curLayer = layers[i];
			// Print the layer identity
			sb.append("Layer ").append(i).append("\n");
			// For each node in the layer
			for (int j = 0; j < curLayer.getNodes().length; j++) {
				Node curNode = curLayer.getNodes()[j];
				// Print the node Identity and associated weights
				sb.append("    Node ").append(j).append("    ");
				// For each weight in the node
				for (int k = 0; k < curNode.getWeights().length; k++) {
					// Print the weights
					sb.append("(").append(df.format(curNode.getWeights()[k])).append(") ");
				}
				sb.append("\n");
			}
		}
		
		
		return sb.toString();
	}
	
	
	/**
	 * @see Thread.run()
	 */
	@Override
	public void run() {
		train();
	}

	/**
	 * A method to kill the thread manually in the event that it does not stop
	 * running on its own.  Slip something in the thread's food... so to speak.
	 */
	public void kill() { 
		poisonPill = true; 
	}
	
	//******************************************
	//************RUN PROGRAM*******************
	//******************************************

	public static void main(String[] args) {
		// Create the network
		NeuralNetwork net = new NeuralNetwork();

		// *** Paste material from comments below

		// Possible inputs for an XOR
		double[][] input = new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 1.0 }, 
				{ 0.0, 1.0, 0.0 }, { 0.0, 1.0, 1.0 }, { 1.0, 0.0, 0.0 },
				{ 1.0, 0.0, 1.0 }, { 1.0, 1.0, 0.0 }, { 1.0, 1.0, 1.0 }};
		// Expected outputs for an XOR
		double[][] expected = new double[][] { { 1.0 }, { 0.0 }, { 1.0 }, 
				{ 0.0 }, { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
		// Neurons/layer count
		int[] neuronCounts = new int[] {3, 4, 1};

		// *** END paste material... ******

		// Create the network
		net.setupNetwork(neuronCounts, input, expected, 0.7, 0.001, 500000);

		// Start the thread
		net.start();

		// Wait for the thread to finish
		try {
			net.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\nTraining is complete.");
		System.out.println("Total Error is " + net.getTotalError());

		// Print out a test result
		System.out.println("\nTest trained network:");
		for (int i = 0; i < expected.length; i++) {
			final double actual[] = net.computeOutput(input[i]);
			System.out.println(input[i][0] + "," + input[i][1]
					+ ", computed: " + new DecimalFormat("0.00").format(actual[0]) 
					+ ", expected: " + expected[i][0]);
		}
		
		System.out.println("\n" + net.toString());
	}
}
