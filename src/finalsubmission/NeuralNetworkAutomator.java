package finalsubmission;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The intent of this class is to
 * 	Generate a random set of sample inputs
 * 	Generate a few networks with various configurations
 * 	Train each network with the same sample inputs and print results
 * 
 * @author Geoff Berl
 */
public class NeuralNetworkAutomator {

	public static final int[] sampleSizes = new int[] { 
		201, 261, 321, 381, 441, 501, 561, 
		621, 681, 741, 801, 861, 921, 981 }; 
	public static final int[] hiddenLayerSizes = 
			new int[] { 4, 5, 6, 7, 8, 9, 10 };
	public static final double[] learningRateRange = new double[] {
		0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5 };
	private static int inputCnt = TextAnalyzer.ATTRIBUTE_PATTERNS.length;
	
	public static void main(String[] args) throws InterruptedException {

		int choice = 1;

		switch (choice) {
		case 0:
			testLearningRates();
			break;
		case 1:
			testNewDataOnNetwork();
			break;
		case 2:
			testManualEntry();
			break;
		default:
			break;
		}
	}

	public static void testLearningRates() throws InterruptedException {
		// Variables
		double minError = 0.01;
		long maxEpochs = 100000;
		
		
		// Generate sample inputs
		TrainingSetGenerator ta = new TrainingSetGenerator(381, "lib/");

		// Training Data
		double[][] input = ta.getInputData();
		double[][] expected = ta.getExpectedOutputs();

		// Ask the user for a learning rate, then run each network
		Scanner in = new Scanner(System.in);
		String userInput = "";
		try {
			System.out.print("Enter a learning rate: ");
			while(!(userInput = in.nextLine()).isEmpty()) {
				// Convert input to double
				double rate = Double.parseDouble(userInput);

				// run each network with the learning rate and print results
				NeuralNetwork net1 = new NeuralNetwork(
						new int[] {inputCnt, 8, 3} , input, expected, rate, minError, maxEpochs);
				net1.start();
				net1.join();
				System.out.println("Results for net1\n" + net1 + "\n");

				NeuralNetwork net2 = new NeuralNetwork(
						new int[] {inputCnt, 7, 3} , input, expected, rate, minError, maxEpochs);
				net2.start();
				net2.join();
				System.out.println("Results for net2\n" + net2 + "\n");

				NeuralNetwork net3 = new NeuralNetwork(
						new int[] {inputCnt, 6, 3} , input, expected, rate, minError, maxEpochs);
				net3.start();
				net3.join();
				System.out.println("Results for net3\n" + net3 + "\n");

				NeuralNetwork net4 = new NeuralNetwork(
						new int[] {inputCnt, 5, 3} , input, expected, rate, minError, maxEpochs);
				net4.start();
				net4.join();
				System.out.println("Results for net4\n" + net4 + "\n");

				NeuralNetwork net5 = new NeuralNetwork(
						new int[] {inputCnt, 4, 3} , input, expected, rate, minError, maxEpochs);
				net5.start();
				net5.join();
				System.out.println("Results for net5\n" + net5 + "\n");

				System.out.println("");
				System.out.print("Enter a learning rate: ");
			}
		} finally {
			in.close();
		}
	}

	public static void testNewDataOnNetwork() throws InterruptedException {
		// Generate sample inputs
		TrainingSetGenerator trainer = new TrainingSetGenerator(561, "lib/");

		// Training Data
		double[][] input = trainer.getInputData();
		double[][] expected = trainer.getExpectedOutputs();

		// Create and train the network
		// run each network with the learning rate and print results
		System.out.println("Creating and training the network...");
		NeuralNetwork net = new NeuralNetwork(
				new int[] {inputCnt, 7, 3} , input, expected, 0.78, 0.001, 100000);
		net.start();
		net.join();
		System.out.println("Results for new data\n" + net + "\n");
		
		// Ask the user for a learning rate, then run each network
		Scanner in = new Scanner(System.in);
		TrainingSetGenerator newData;
		try {
			System.out.print("Run New Data (hit enter to end)? ");
			while(!(in.nextLine()).isEmpty()) {

				// Generate sample inputs from a different text base
				newData = new TrainingSetGenerator(300, "newlib/");

				// New sample inputs
				double[][] testInput = newData.getInputData();
				double[][] testExpected = newData.getExpectedOutputs();

				// Test the network with the new sample data
				System.out.println("\nTest trained network:");

				for (int i = 0; i < testInput.length; i++) {
					
					// Get the computed output
					final double actual[] = net.computeOutput(testInput[i]);

					
					System.out.println(trainer.getWinner(testExpected[i]) 
							+ "\t" + trainer.getWinner(actual) + "\t" 
							+ (trainer.getWinner(testExpected[i]).equals(trainer.getWinner(actual)) == true ? 1 : 0));

				}
				
				// Ask for next task
				System.out.println("");
				System.out.print("Run New Data? ");
			}
		} finally {
			in.close();
		}
	}
	
	public static void testManualEntry() throws InterruptedException {
		// NOTE: Needs to be a number divisible by 3  eh.
		TrainingSetGenerator ta = new TrainingSetGenerator(381, "lib/");

		// Training Data
		double[][] input = ta.getInputData();
		double[][] expected = ta.getExpectedOutputs();

		// Create the network
		System.out.println("Training the network...");
		NeuralNetwork net = new NeuralNetwork(
				new int[] {inputCnt, 7, 3} , input, expected, 0.78, 0.001, 100000);

		// Start the thread and wait for the thread to finish
		net.start();
		net.join();

		System.out.println("\nTraining is complete.");
		System.out.println("Total Error is " + net.getTotalError());

		// Print out a test result
		System.out.println("\nTest trained network:");
		
		System.out.println("\n" + net.toString());

		// now let the user enter some data to test the network
		Scanner in = new Scanner(System.in);
		ExecutorService executor = Executors.newFixedThreadPool(3);
		String userInput = "";
		try {
			System.out.print("Enter a string: ");
			while(!(userInput = in.nextLine()).isEmpty()) {
				
				
				
				// Analyze the text
				TextAnalyzer textAnalyzer = new TextAnalyzer("Manual",userInput);
				Future<TextAnalysisResult> analysis = executor.submit(textAnalyzer);
				double[] attrib = null;
				try {
					attrib = analysis.get().getAttributes();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				ta.normalizeInput(attrib);
				
				// Run entry through network
				final double[] result = net.computeOutput(attrib);
				
				// Print the Expected values
				System.out.println("Your input (" + userInput + ") results are as follows:");
				System.out.print("Calculated Class: " + ta.getWinner(result));

				// Ask for another entry
				System.out.println("");
				System.out.print("Enter a string: ");
			}
		} finally {
			in.close();
		}

		executor.shutdown();

	}
}
