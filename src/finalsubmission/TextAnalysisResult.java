package finalsubmission;

/**
 * TextAnalysisResult.java
 * 
 * This class represents a classification and results array to be used with
 * the TextAnalyzer object.
 * 
 * @author Geoff Berl
 */
public class TextAnalysisResult {
	private String id;
	private double[] attributes;
	private String originalString;
	
	/**
	 * Three args constructor
	 * @param id	The classification id
	 * @param attributes	the resulting count per attribute
	 * @param originalString	the original string that was analyzed
	 */
	public TextAnalysisResult(String id, double[] attributes, String originalString) {
		super();
		this.id = id;
		this.attributes = attributes;
		this.originalString = originalString;
	}

	/**
	 * Get the classification id for the string passed in.
	 * @return	the classification id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get the array of attribute counts
	 * @return the array of attribute counts
	 */
	public double[] getAttributes() {
		return attributes;
	}

	/**
	 * Get the original string that was passed in to be analyzed
	 * @return the original string analyzed
	 */
	public String getOriginalString() {
		return originalString;
	}

}
