package finalsubmission;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * TrainingSetGenerator.java
 * 
 * The goal of this class is to, given a number of samples...
 * 	Create a training set with that many samples
 * 	Create numNetworks ANNs with varying hidden layer node counts
 * 	Train said networks with numVariations varied learning rates
 * 	Compile the results for each ANN and it's varying results
 * 
 * This should allow us to figure out two things
 * 	If this sample size is large enough
 * 	Which network variation results in the best outcome (will need to be replicated to prove)
 * 
 * @author Geoff Berl
 */
public class TrainingSetGenerator {

	final private static boolean NORMALIZE_VALUES = true;
	final private static int THREAD_POOL_SIZE = 4;
	final private ExecutorService executor;
	final private ExecutorCompletionService<TextAnalysisResult> completionService;
	private double[][] inputData;
	private double[][] expectedOutput;
	private double[][] minimax;
	public static final String[] CLASSIFIERS = { "Dutch", "Italian", "English" };
	private String folder;

	public TrainingSetGenerator(int numSamples, String folder) {
		// Create executor and wrap in a completion service
		executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		completionService = new ExecutorCompletionService<TextAnalysisResult>(executor);
		this.folder = folder;

		// Ensure that the executor shuts down properly if the
		// application is closed while the threads are running.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				executor.shutdown();
				while (true) {
					try {
						// Wait for the service to exit
						if (executor.awaitTermination(5, TimeUnit.SECONDS)) {
							break;
						}
					} catch (InterruptedException ignored) {
					}
				}
				System.out.println("Closing now");
			}
		}));

		// Create training sample data
		inputData = new double[numSamples][TextAnalyzer.ATTRIBUTE_PATTERNS.length];
		expectedOutput = new double[numSamples][3];
		setupSampleData(numSamples);
	}

	private void setupSampleData(int nSamples) {
		//	Get TraininigData for each
		SampleInput[] sampleData = new SampleInput[3];
		sampleData[0] = new SampleInput("du", folder);
		sampleData[1] = new SampleInput("it", folder);
		sampleData[2] = new SampleInput("en", folder);	

		// For each sample set, generate nSamples
		String[] sampleInputs = new String[nSamples];
		String[] sampleClasses = new String[nSamples];	
		int segmentSize = (nSamples/sampleData.length);
		for (int i = 0; i < sampleData.length; i++) {
			int offset = segmentSize*i;
			for (int j = 0; j < segmentSize; j++) {
				// Get a sample string from 10 to 100 characters
				sampleInputs[j + offset] = sampleData[i].getSample(10, 100);
				sampleClasses[j + offset] = sampleData[i].getIdentity();
			}
		}
		//		System.out.println("We have training data containing " + sampleInputs.length + " records");

		// Create and submit the search threads to get attributes
		for (int i = 0; i < sampleInputs.length; i++) {
			completionService.submit(new TextAnalyzer(sampleClasses[i], sampleInputs[i]));
		}

		/* Loop through and get results */
		minimax = new double[inputData[0].length][2];
		for (int i = 0; i < sampleInputs.length; i++) {
			Future<TextAnalysisResult> future = null;
			try {
				// Wait for a result
				future = completionService.take();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			// Retrieve the count of occurrences
			TextAnalysisResult result = null;
			try {
				result = future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			// Get minimax values
			for (int j = 0; j < minimax.length; j++) {
				if (result.getAttributes()[j] < minimax[j][0]) {
					minimax[j][0] = result.getAttributes()[j];
				}
				if (result.getAttributes()[j] > minimax[j][1]) {
					minimax[j][1] = result.getAttributes()[j];
				}
			}

			// Add input array
			inputData[i] = result.getAttributes();

			// Generate and add output array
			double[] expOut = new double[sampleData.length];
			for (int j = 0; j < expOut.length; j++) {
				if (result.getId().equals(sampleData[j].getIdentity())) {
					expOut[j] = 1;
				} else {
					expOut[j] = 0;
				}
			}
			expectedOutput[i] = expOut;
		}

		if (NORMALIZE_VALUES) {
			// Now we must loop through all of the data to normalize it
			for (int i = 0; i < inputData.length; i++) {
				normalizeInput(inputData[i]);
			}
		}

		// The executor can safely be shut down.
		executor.shutdown();
	}

	public double[][] getInputData() {
		return this.inputData;
	}

	public double[][] getExpectedOutputs() {
		return this.expectedOutput;
	}
	
	public void normalizeInput(double[] input) {
		// Normalize each feature
		for (int i = 0; i < input.length; i++) {
			input[i] = (input[i]-minimax[i][0])/(minimax[i][1]-minimax[i][0]);
		}
	}

	public String getWinner(double[] output) {
		double winnerValue = 0;
		int winningID = -1;
		for(int i = 0; i < output.length; i++) {
			if (output[i] > winnerValue) {
				winnerValue = output[i];
				winningID = i;
			}
		}
		return CLASSIFIERS[winningID];
	}
}
