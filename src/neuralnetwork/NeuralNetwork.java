package neuralnetwork;

import java.util.ArrayList;
import java.util.List;

public class NeuralNetwork {

	List<NetworkLayer> layers;
	
	public NeuralNetwork() {
		layers = new ArrayList<NetworkLayer>();
	}
	
	/**
	 *	function BACK-PROP-LEARNING(examples,network) returns a neural network
	 *	inputs: examples, a set of examples, each with input vector x and output vector y
	 *			network , a multilayer network with L layers, weights wi,j , activation function g
	 *	local variables: ∆, a vector of errors, indexed by network node
	*/
	public void train(float[] trainingSet) {

//		repeat
//			for each weight wi,j in network do
//				wi,j ← a small random number
//			for each example (x, y) in examples do
//				/* Propagate the inputs forward to compute the outputs */ 
//				for each node i in the input layer do
//					ai ←xi
//				for l = 2 to L do
//					for each node j in layer l do 
//						inj ←􏰀i wi,j ai
//						aj ←g(inj)
//				/* Propagate deltas backward from output layer to input layer */ 
//				for each node j in the output layer do
//					∆[j]←g′(inj) × (yj − aj) 
//				for l = L − 1 to 1 do
//					for each node i in l􏰀ayer l do 
//						∆[i] ← g′(ini) j wi,j ∆[j]
//				/* Update every weight in network using deltas */ 
//				for each weight wi,j in network do
//					wi,j←wi,j + α × ai × ∆[j] 
//		until some stopping criterion is satisfied
//		return network
		
		/** Pseudo from online tutorial */
//		Assign all network inputs and output 
//		Initialize all weights with small random numbers, typically between -1 and 1 
//
//		repeat 
//		    for every pattern in the training set 
//		        Present the pattern to the network 
//
////		        Propagate the input forward through the network: 
//		            for each layer in the network  
//		                for every node in the layer  
//		                    1. Calculate the weight sum of the inputs to the node  
//		                    2. Add the threshold to the sum  
//		                    3. Calculate the activation for the node  
//		                end  
//		            end 
//
////		        Propagate the errors backward through the network 
//		             for every node in the output layer  
//		                calculate the error signal  
//		            end 
//
//		            for all hidden layers  
//		                for every node in the layer  
//		                    1. Calculate the node's signal error  
//		                    2. Update each node's weight in the network  
//		                end  
//		            end 
//
////		        Calculate Global Error 
//		            Calculate the Error Function 
//
//		    end 
//
//		while ((maximum  number of iterations < than specified) AND  
//		          (Error Function is > than specified))
	}
}
