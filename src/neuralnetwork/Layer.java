package neuralnetwork;

public class Layer {

	private Node[] nodes;	// The nodes for this layer
	private double input[];	// Vector of inputs signals from previous layer to the current layer
	private double output;	// Output for this layer

	/**
	 * Two arg constructor creates a feed-forward network layer
	 * @param numNodes the number of nodes in the layer
	 * @param numInputs the number of inputs from previous layer
	 */
	public Layer(int numNodes, int numInputs) {
		// Create an array for the nodes
		nodes = new Node[numNodes];

		// Initialize the nodes with the number of inputs
		for (int i = 0; i < numNodes; i++) {
			nodes[i] = new Node(numInputs);
		}

		// Create the array of inputs from previous layer
		input = new double[numInputs];
	}

	/**
	 * Calculates the outputs for nodes in the layer
	 */
	public void feedForward() {

		for (int i = 0; i < nodes.length; i++) {
			// Get the current node's threshold
			output = nodes[i].getThreshold();

			// For each weight in the node, compute the change
			for (int j = 0; j < nodes[i].getWeights().length; j++) {
				output = output + input[j] * nodes[i].getWeights()[j];
			}

			// Set the outputs of the node using sigmoid function [g(x)]
			nodes[i].setOutput(1/(1+Math.exp(-output)));
		}
	}

	/**
	 * Get an array of outputs from this layer
	 * @return an array of outputs
	 */
	public double[] getOutputs() {

		double[] retVal;

		retVal = new double[nodes.length];

		for (int i=0; i < nodes.length; i++)
			retVal[i] = nodes[i].getOutput();

		return (retVal);
	}

	public Node[] getNodes() {
		return nodes;
	}

	public double[] getInput() {
		return input;
	}

	public void setInput(double[] input) {
		this.input = input;
	}

	public double getOutput() {
		return output;
	}

	public void setOutput(double output) {
		this.output = output;
	}

	public void setNodes(Node[] nodes) {
		this.nodes = nodes;
	}
	
	public int getNumberOfNodes() {
		return nodes.length;
	}
}
