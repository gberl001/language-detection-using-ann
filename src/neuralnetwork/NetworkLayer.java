package neuralnetwork;

import java.util.Random;

public class NetworkLayer {

	final private float[] weights;
	final private float[] thresholds;
	NetworkLayer forward;
	NetworkLayer backward;
	
	public NetworkLayer(int input, int output) {
		
		this.weights = new float[output];
		this.thresholds = new float[input];
	}
	
	
	public void initializeWeights() {
		Random rand = new Random();
		for (int i = 0; i < weights.length; i++) {
			weights[i] = rand.nextFloat()*2.0f - 1.0f;
		}
	}
	
//	// The FeedForward function is called so that 
//	// the outputs for all the nodes in the current 
//	// layer are calculated
//	public void FeedForward() {
//		for (int i = 0; i < Node.length; i++) {
//			Net = Node[i].threshold;
//
//			for (int j = 0; j < Node[i].weights.length; j++)
//				Net = Net + Input[j] * Node[i].weights[j];
//
//			Node[i].output = Sigmoid(Net);
//		}
//	}
	
	
	//****************************************************\\
	//*******************SETTERS**************************\\
	//****************************************************\\
	public void setNextLayer(NetworkLayer layer) {
		this.forward = layer;
	}
	
	public void setPreviousLayer(NetworkLayer layer) {
		this.backward = layer;
	}
	
}
