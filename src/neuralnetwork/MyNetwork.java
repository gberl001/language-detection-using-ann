package neuralnetwork;

import java.text.DecimalFormat;

/**
 * Network.java
 * 
 * A class to simulate a Feed-forward network using backpropagation learning
 * techniques.  This class is also a thread, really just for training purposes
 * so that we can run multiple cases to figure out the best case we can.
 * 
 * @author Geoff Berl
 */
public class MyNetwork extends Thread {

	public static int NUM_LAYERS = 3; 	// Number of layers (3 for the lab)

	private double	minError;			// The smallest acceptable error
	private double[][]	expectedOutput;	// Expected outputs with respect to training data inputs
	private double[][] trainingData;	// The input data from which to train

	private double	learningRate;		// The learning rate (try many different)

	private long	maxEpochs;			// Stop after this many epochs
	public Layer[] layers;				// The layers of the network
	public double[][] actualOutput;		// Stored output for computing total error
	private boolean poisonPill = false; // A "notification" that the thread should stop

	public MyNetwork() {
		super();
		// Setup will be called later
	}

	public  MyNetwork(int[] nodesPerLayer, double[][] inputSamples, 
			double outputSamples[][], double learnRate, double minError, 
			long maxEpochs) {
		this();
		// Set up the network since they used th full constructor
		setupNetwork(nodesPerLayer, inputSamples, outputSamples,
				learnRate, minError, maxEpochs);
	}

	/**
	 * Call this class if you initialized a no args Network.  You cannot run
	 * the training algorithm without calling this first.  This is called
	 * automatically if you chose to initialize the Network using the 
	 * parameterized constructor so there is no need to call again.  However,
	 * calling this again will not have ill-effects.
	 * @param numNodesInLayer	an array containing the number of nodes for each layer
	 * @param inputSamples		the provided training data
	 * @param expectedOutputs	the expected outputs from training data
	 * @param learnRate			the rate at which the network learns
	 * @param minError			the error to beat
	 * @param maxEpochs			the maximum number of epochs to perform
	 */
	public void setupNetwork(int[] numNodesInLayer, double[][] inputSamples, 
			double[][] expectedOutputs, double learnRate, double minError, 
			long maxEpochs) {

		/** Check validity before continuing */
		if (!validateNetworkData(numNodesInLayer, inputSamples, expectedOutputs)) {
			return;
		}

		// Set up the variables
		this.minError = minError;
		this.learningRate = learnRate;
		this.maxEpochs = maxEpochs;
		layers = new Layer[NUM_LAYERS];
		this.expectedOutput = expectedOutputs;
		this.trainingData = inputSamples;
		// Create an array to hold the computed output values.
		actualOutput = new double[inputSamples.length][expectedOutput[0].length];

		// Assign number of nodes to each layer
		for (int i = 0; i < NUM_LAYERS; i++)  {
			switch(i) {
			case 0: 
				// The input layer has equally many inputs as outputs.
				layers[i] = new Layer(numNodesInLayer[i], numNodesInLayer[i]);
				break;
			default:
				layers[i] = new Layer(numNodesInLayer[i], numNodesInLayer[i-1]);
				break;
			}
		}

	}

	/*
	 * Ensure that the data sent in is not going to cause a problem
	 */
	private boolean validateNetworkData(int[] numNodesInLayer, 
			double[][] inputSamples, double[][] expectedOutputs) {
		// Node counts
		if (numNodesInLayer.length < NUM_LAYERS) {
			System.err.println("You did not provide enough node counts");
			return false;
		} else if (numNodesInLayer.length > NUM_LAYERS) {
			System.err.println("You provided too many node counts");
			return false;
		}
		// Input samples size
		if (inputSamples[0].length < numNodesInLayer[0]) {
			System.err.println("Your sample input doesn't have enough fields.");
			return false;
		} else if (inputSamples[0].length > numNodesInLayer[0]) {
			System.err.println("Your sample input has too many fields.");
			return false;
		}
		// Expected outputs size 
		if (expectedOutputs[0].length < numNodesInLayer[NUM_LAYERS-1]) {
			System.err.println("Your expected output doesn't have enough fields.");
			return false;
		} else if (expectedOutputs[0].length > numNodesInLayer[NUM_LAYERS-1]) {
			System.err.println("Your expected output has too many fields.");
			return false;
		}
		return true;
	}

	/**
	 * Computes the activation function for each node in the array
	 */
	public void feedForward(){
		// TODO: Update comments for this method
		// Since no weights contribute to the output from the input layer, 
		// assign the input from the input layer to all the nodes in the 
		// first hidden layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getNodes()[i].setOutput(layers[0].getInput()[i]);
		}
		layers[1].setInput(layers[0].getInput());

		// For the remaining layers, call feed forward for the layer (i)
		// to compute it's outputs and then assign those outputs to the next 
		// layer (i+1).
		for (int i = 1; i < NUM_LAYERS; i++) {
			layers[i].feedForward();

			// When we reach the end, there are no more layers to insert outputs to.
			if (i != NUM_LAYERS-1) {
				layers[i+1].setInput(layers[i].getOutputs());
			}
		}

	}

	/**
	 * A method to return a result from a single input.  Typically this would 
	 * be used once the network is trained.
	 * @param input	the single input to analyze
	 * @return	an array containing the output values.
	 */
	public double[] computeOutput(double[] input) {
		double[] retVal = new double[layers[NUM_LAYERS-1].getNumberOfNodes()];

		// Set the inputs for the input layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getInput()[i] = input[i];
		}

		// Calculate activation values
		feedForward();

		// Get the outputs
		for (int i = 0; i < layers[NUM_LAYERS-1].getNumberOfNodes(); i++) {
			retVal[i] = layers[NUM_LAYERS-1].getNodes()[i].getOutput();
		}

		return retVal;
	}

	/*
	 * Computes the error (for a single iteration) for each node in the network
	 */
	private void computeNodeErrors(double[] expected) {

		int opLayer;

		opLayer = NUM_LAYERS-1;

		// Calculate error for output layer nodes
		double opNodeActual;
		for (int i = 0; i < layers[opLayer].getNumberOfNodes(); i++) {
			opNodeActual = layers[opLayer].getNodes()[i].getOutput();

			// TODO: This is supposed to be the g'(x) calculation
			// Calculating the output layer error
			layers[opLayer].getNodes()[i].setError(
					(expected[i] - opNodeActual) * opNodeActual 
					* (1-opNodeActual));
		}

		// Calculate error for nodes in other layers
		double deltaK;
		for (int i = opLayer-1; i > 0; i--) {
			// For every node in the layer...
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				deltaK = 0;

				// add up the weights for every node in the next (output) layer
				// Known as delta[k] for the output layer
				for (int k = 0; k < layers[i+1].getNumberOfNodes(); k++) {
					deltaK += layers[i+1].getNodes()[k].getWeights()[j] 
							* layers[i+1].getNodes()[k].getError();
				}

				// Set the error for this node (O[ij]*(1-O[ij])*w[kj])
				// Known as delta[k] for a hidden layer
				layers[i].getNodes()[j].setError(
						layers[i].getNodes()[j].getOutput() 
						* (1 - layers[i].getNodes()[j].getOutput())
						* deltaK);

			}
		}

	}

	/*
	 * Propagates the error through the network (backwards of course)
	 */
	private void BackPropagateError() {

		// Update weights for every layer
		for (int i = NUM_LAYERS-1; i > 0; i--) {
			// Update weights for every node in the layer
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				// Update weights
				for (int k = 0; k < layers[i-1].getNumberOfNodes(); k++) {

					// Get the difference between the weights (delta k)
					double weightDiff = learningRate 
							* layers[i].getNodes()[j].getError() 
							* layers[i-1].getNodes()[k].getOutput();

					// Update the weight weight
					layers[i].getNodes()[j].getWeights()[k] += weightDiff;
				}
			}
		}
	}

	/*
	 * Returns the total error for the network
	 */
	public double getTotalError() {

		double totalError = 0;
		// For each sample...
		for (int i = 0; i < trainingData.length; i++) {
			// For each node
			for (int j = 0; j < layers[NUM_LAYERS-1].getNumberOfNodes(); j++) {
				totalError += Math.pow((expectedOutput[i][j] - actualOutput[i][j]),2);
			}
		}
		return totalError*0.5;
	}

	/**
	 * A method to train the network.  This method will run through the 
	 * activation function, error calculation and weight updates for each
	 * sample case.  The training stops when the max epochs count is reached or
	 * the error is less than the minimum provided error.  The computation may
	 * also be forced to stop by setting a "poisonPill" object to true.
	 */
	public void trainNetwork() {

		long currentEpoch = 0;
		while (!poisonPill && (getTotalError() > minError) 
				&& (currentEpoch < maxEpochs)) {

			// For each input record
			for (int in = 0; in < trainingData.length; in++) {
				// Get the sample data for this iteration
				double[] currentInput = trainingData[in];
				double[] currentExpected = expectedOutput[in];

				// Set the inputs for the input layer
				for (int j = 0; j < layers[0].getNumberOfNodes(); j++) {
					layers[0].getInput()[j] = currentInput[j];
				}

				// Always move forward... no matter what happens
				feedForward();

				// keep track of outputs for calculating the total error.
				for (int j = 0; j < layers[NUM_LAYERS-1].getNumberOfNodes(); j++) {
					actualOutput[in][j] = 
							layers[NUM_LAYERS-1].getNodes()[j].getOutput();
				}

				// Call a training iteration
				computeNodeErrors(currentExpected);
				BackPropagateError();
			}
			// Print out some helpful information
			System.out.println("Epoch " + currentEpoch + 
					": Error=" + getTotalError());
			//			System.out.println("!false:" + !poisonPill 
			//					+ " && " + "(" + getTotalError() + " > " + minError 
			//					+ ") " + (getTotalError() > minError) +
			//					" && " + "(" + currentEpoch + " < " + maxEpochs + ")" 
			//					+ (currentEpoch < maxEpochs));
			//			System.out.println("Total Error: " + getTotalError() + " (>" + MinimumError + "?)");
			currentEpoch++; // Increment epoch count
		} 

	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.00");
		
		// For each layer
		for (int i = 0; i < layers.length; i++) {
			Layer curLayer = layers[i];
			// Print the layer identity
			sb.append("Layer ");
			sb.append(i);
			sb.append("\n");
			// For each node in the layer
			for (int j = 0; j < curLayer.getNodes().length; j++) {
				Node curNode = curLayer.getNodes()[j];
				// Print the node Identity and associated weights
				sb.append("    Node ");
				sb.append(j);
				sb.append("    ");
				// For each weight in the node
				for (int k = 0; k < curNode.getWeights().length; k++) {
					// Print the weights
					sb.append("(");
					sb.append(df.format(curNode.getWeights()[k]));
					sb.append(") ");
				}
				sb.append("\n");
			}
		}
		
		
		return sb.toString();
	}
	
	
	@Override
	public void run() {
		trainNetwork();
	}

	// Slip something in the thread's food... so to speak.
	public void kill() { 
		poisonPill = true; 
	}
	
	//******************************************
	//************RUN PROGRAM*******************
	//******************************************

	public static void main(String[] args) {
		MyNetwork net = new MyNetwork();

		// *** Paste material from comments below

		// Possible inputs for an XOR
		double[][] input = new double[][] { { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 1.0 }, 
				{ 0.0, 1.0, 0.0 }, { 0.0, 1.0, 1.0 }, { 1.0, 0.0, 0.0 },
				{ 1.0, 0.0, 1.0 }, { 1.0, 1.0, 0.0 }, { 1.0, 1.0, 1.0 }};
		// Expected outputs for an XOR
		double[][] expected = new double[][] { { 1.0 }, { 0.0 }, { 1.0 }, 
				{ 0.0 }, { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
		// Neurons/layer count
		int[] neuronCounts = new int[] {3, 4, 1};

		// *** END paste material... ******

		// Create the network
		net.setupNetwork(neuronCounts, input, expected, 0.7, 0.001, 500000);

		// Start the thread
		net.start();

		// Wait for the thread to finish
		try {
			net.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\nTraining is complete.");
		System.out.println("Total Error is " + net.getTotalError());

		// Print out a test result
		System.out.println("\nTest trained network:");
		for (int i = 0; i < expected.length; i++) {
			final double actual[] = net.computeOutput(input[i]);
			System.out.println(input[i][0] + "," + input[i][1]
					+ ", computed: " + new DecimalFormat("0.00").format(actual[0]) 
					+ ", expected: " + expected[i][0]);
		}
		
		System.out.println("\n" + net.toString());
	}
}
