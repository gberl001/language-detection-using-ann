package neuralnetwork;

/**
 * Network.java
 * 
 * A class to simulate a Feed-forward network using backpropagation learning
 * techniques.  This class is also a thread, really just for training purposes
 * so that we can run multiple cases to figure out the best case we can.
 * 
 * @author Geoff Berl
 */
public class Network extends Thread {

	public static int NUM_LAYERS = 3; // Number of layers (3 for the lab)
	//	private double	totalError;	// The total network error (we want this to be zero)

	// The minimum Error Function defined by the user
	private double	MinimumError;

	// The user-defined expected output pattern for a set of samples
	private double[][]	expectedOutput;

	// The user-defined input pattern for a set of samples
	private double[][] trainingData;

	private double	learnRate;	// The learning rate (try many different)

	// Number of training sets
	private  int	sampleCount;

	// Current training set/sample that is used to train network
	private  int	sample;

	private long	maxEpochs;	// Stop after this many epochs

	// Public Variables
	public Layer[] layers;

	public double[][] actualOutput;
	private boolean poisonPill = false;

	//	Welcome2 parent;
	//	long delay = 0;
	//	boolean die = false;

	public Network() {
		super();
		// Setup later
	}
	
	public  Network(int[] nodesPerLayer, double[][] inputSamples, 
			double outputSamples[][], double learnRate, double minError, 
			long maxEpochs) {
		this();
		// Set up the network since they used th full constructor
		setupNetwork(nodesPerLayer, inputSamples, outputSamples,
				learnRate, minError, maxEpochs);
	}
	
	public void setupNetwork(int[] numNodesInLayer, double[][] inputSamples, 
			double outputSamples[][], double learnRate, double minError, 
			long maxEpochs) {
		// Initiate variables
		sampleCount = inputSamples.length;
		this.MinimumError = minError;
		this.learnRate = learnRate;
		this.maxEpochs = maxEpochs;

		// Create network layers
		layers = new Layer[NUM_LAYERS];

		// Assign the number of node to the input layer
		layers[0] = new Layer(numNodesInLayer[0],numNodesInLayer[0]);

		// Assign number of nodes to each layer
		for (int i = 1; i < NUM_LAYERS; i++)  {
			layers[i] = new Layer(numNodesInLayer[i],numNodesInLayer[i-1]);
		}

		trainingData = new double[sampleCount][layers[0].getNumberOfNodes()];
		expectedOutput = new double[sampleCount][layers[NUM_LAYERS-1].getNumberOfNodes()];
		actualOutput = new double[sampleCount][layers[NUM_LAYERS-1].getNumberOfNodes()];

		// Assign input set
		for (int i = 0; i < sampleCount; i++) {
			for (int j = 0; j < layers[0].getNumberOfNodes(); j++) {
				trainingData[i][j] = inputSamples[i][j];
			}
		}

		// Assign output set
		for (int i = 0; i < sampleCount; i++) {
			for (int j = 0; j < layers[NUM_LAYERS-1].getNumberOfNodes(); j++) {
				expectedOutput[i][j] = outputSamples[i][j];
			}
		}
	}

	// Calculate the node activations
	public void feedForward(){

		// Since no weights contribute to the output from the input layer, 
		// assign the input from the input layer to all the nodes in the 
		// first hidden layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getNodes()[i].setOutput(layers[0].getInput()[i]);
		}
		layers[1].setInput(layers[0].getInput());

		// For the remaining layers, call feed forward for the layer (i)
		// to compute it's outputs and then assign those outputs to the next 
		// layer (i+1).
		for (int i = 1; i < NUM_LAYERS; i++) {
			layers[i].feedForward();

			// When we reach the end, there are no more layers to insert outputs to.
			if (i != NUM_LAYERS-1) {
				layers[i+1].setInput(layers[i].getOutputs());
			}
		}

	}

	public double[] computeOutput(double[] input) {
		double[] retVal = new double[layers[NUM_LAYERS-1].getNumberOfNodes()];

		// Set the inputs for the input layer
		for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
			layers[0].getInput()[i] = input[i];
		}

		// Calculate activations
		feedForward();

		// Get the outputs
		for (int i = 0; i < layers[NUM_LAYERS-1].getNumberOfNodes(); i++) {
			retVal[i] = layers[NUM_LAYERS-1].getNodes()[i].getOutput();
		}

		return retVal;
	}

	/**
	 * The main backpropagation method, it calls to compute the perceptron
	 * errors, then backpropagate them through the network.
	 */
	public void iterateBackProp() {
		computeNodeErrors();
		BackPropagateError();
	}


	private void computeNodeErrors() {

		int opLayer;

		opLayer = NUM_LAYERS-1;

		// Calculate error for output layer nodes
		double opNodeActual;
		for (int i = 0; i < layers[opLayer].getNumberOfNodes(); i++) {
			opNodeActual = layers[opLayer].getNodes()[i].getOutput();

			// TODO: This is supposed to be the g'(x) calculation
			// Calculating the output layer error
			layers[opLayer].getNodes()[i].setError(
					(expectedOutput[sample][i] - opNodeActual) 
					* opNodeActual * (1-opNodeActual));
		}

		// Calculate error for nodes in other layers
		double deltaK;
		for (int i = opLayer-1; i > 0; i--) {
			// For every node in the layer...
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				deltaK = 0;

				// add up the weights for every node in the next (output) layer
				// Known as delta[k] for the output layer
				for (int k = 0; k < layers[i+1].getNumberOfNodes(); k++) {
					deltaK += layers[i+1].getNodes()[k].getWeights()[j] 
							* layers[i+1].getNodes()[k].getError();
				}

				// Set the error for this node (O[ij]*(1-O[ij])*w[kj])
				// Known as delta[k] for a hidden layer
				layers[i].getNodes()[j].setError(
						layers[i].getNodes()[j].getOutput() 
						* (1 - layers[i].getNodes()[j].getOutput())
						* deltaK);

			}
		}

	}

	private void BackPropagateError() {

		// Update weights for every layer
		for (int i = NUM_LAYERS-1; i > 0; i--) {
			// Update weights for every node in the layer
			for (int j = 0; j < layers[i].getNumberOfNodes(); j++) {
				// Update weights
				for (int k = 0; k < layers[i-1].getNumberOfNodes(); k++) {

					// Calculate difference
					//					System.out.println("Calculating weight difference of layer "
					//							+ i + "'s " + j + " node and layer " + (i-1) 
					//							+ "'s " + k + " node");
					double weightDiff = learnRate 
							* layers[i].getNodes()[j].getError() 
							* layers[i-1].getNodes()[k].getOutput();

					// Update weight between node j and k
					layers[i].getNodes()[j].getWeights()[k] = 
							layers[i].getNodes()[j].getWeights()[k] + 
							weightDiff;
				}
			}
		}
	}

	private double getTotalError() {

		double totalError = 0;
		// For each sample...
		for (int i = 0; i < sampleCount; i++) {
			// For each node
			for (int j = 0; j < layers[NUM_LAYERS-1].getNumberOfNodes(); j++) {
				totalError += 0.5 * 
						Math.pow((expectedOutput[i][j] - actualOutput[i][j]),2);
			}
		}

		return totalError;
	}




	public void trainNetwork() {
		// System.out.println("" + MaximumNumberOfIterations);

		/*
			try { System.in.read(); }
			catch(IOException _e) { }
		 */
		long currentEpoch = 0;
		while (!poisonPill && (getTotalError() > MinimumError) 
				&& (currentEpoch < maxEpochs)) {

			// For each input sample
			for (sample = 0; sample < sampleCount; sample++) {
				// Set the inputs for the input layer
				for (int i = 0; i < layers[0].getNumberOfNodes(); i++) {
					layers[0].getInput()[i] = trainingData[sample][i];
				}

				// Always move forward... no matter what happens
				feedForward();

				// keep track of outputs for calculating the total error.
				for (int i = 0; i < layers[NUM_LAYERS-1].getNumberOfNodes(); i++) {
					actualOutput[sample][i] = 
							layers[NUM_LAYERS-1].getNodes()[i].getOutput();
				}

				// Call a training iteration
				iterateBackProp();
			}
			System.out.println("!false:" + !poisonPill 
					+ " && " + "(" + getTotalError() + " > " + MinimumError 
					+ ") " + (getTotalError() > MinimumError) +
					" && " + "(" + currentEpoch + " < " + maxEpochs + ")" 
					+ (currentEpoch < maxEpochs));
			//			System.out.println("Total Error: " + getTotalError() + " (>" + MinimumError + "?)");
			currentEpoch++; // Increment epoch count
		} 

	}

	@Override
	public void run() {
		trainNetwork();
	}

	// Slip something in the thread's food... so to speak.
	public void kill() { 
		poisonPill = true; 
	}

	//******************************************
	//************RUN PROGRAM*******************
	//******************************************

	public static void main(String[] args) {
		Network net = new Network();
		
		// *** Paste material from comments below
		
		// Possible inputs for an XOR
		double[][] input = new double[][] { { 0.0, 0.0 }, { 1.0, 0.0 }, { 0.0, 1.0 }, { 1.0, 1.0 } };
		// Expected outputs for an XOR
		double[][] expected = new double[][] { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
		// Neurons/layer count
		int[] neuronCounts = new int[] {2, 3, 1};
		
		// *** END paste material... ******
	
		// Create the network
		net.setupNetwork(neuronCounts, input, expected, 0.7, 0.001, 50000);

		// Start the thread
		net.start();
		
		// Wait for the thread to finish
		try {
			net.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\nTraining is complete.");
		System.out.println("Total Error is " + net.getTotalError());

		// Print out a test result
		System.out.println("\nTest trained network:");
		for (int i = 0; i < expected.length; i++) {
			final double actual[] = net.computeOutput(input[i]);
			System.out.println(input[i][0] + "," + input[i][1]
					+ ", actual=" + actual[0] + ",expected=" + expected[i][0]);
		}
	}

	// *************XOR material ******
//	// Possible inputs for an XOR
//	double[][] input = new double[][] { { 0.0, 0.0 }, { 1.0, 0.0 }, { 0.0, 1.0 }, { 1.0, 1.0 } };
//	// Expected outputs for an XOR
//	double[][] expected = new double[][] { { 0.0 }, { 1.0 }, { 1.0 }, { 0.0 } };
//	// Neurons/layer count
//	int[] neuronCounts = new int[] {2, 3, 1};
	// END ******** XOR material ******
}
