package neuralnetwork;

import java.util.Random;

public class Node {

	private double output;		// The output
	private double[] weights;	// The weights for this node
	private double threshold;	// The threshold for this node
	private double error;		// The error for this node

	/**
	 * Single arg constructor
	 * @param outputs the number of outputs for this node
	 */
	public Node(int outputs) {
		// Create the weights array
		weights = new double[outputs];

		// Set initial random weights
		initializeWeights();
	}

	/**
	 * Initializes the weights with random values
	 */
	private void initializeWeights() {
		Random rand = new Random(System.currentTimeMillis());

		// Create a random threshold
		threshold = rand.nextDouble()*2.0 - 1.0;

		// Create random weights
		for (int i = 0; i < weights.length; i++) {
			weights[i] = rand.nextDouble()*2.0 - 1.0;
		}
	}


	//****************************************************\\
	//*******************GETTERS**************************\\
	//****************************************************\\

	public double[] getWeights() {
		return weights;
	}

	public double getOutput() {
		return output;
	}

	public double getThreshold() {
		return threshold;
	}

	public double getError() {
		return error;
	}

	//****************************************************\\
	//*******************SETTERS**************************\\
	//****************************************************\\

	public void setOutput(double output) {
		this.output = output;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public void setError(double error) {
		this.error = error;
	}

}
